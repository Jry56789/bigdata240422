package com.atguigu.springbootdemo.userinfo.service;

import com.atguigu.springbootdemo.userinfo.bean.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2024-08-27
 */
public interface OrderInfoService extends IService<OrderInfo> {

}
