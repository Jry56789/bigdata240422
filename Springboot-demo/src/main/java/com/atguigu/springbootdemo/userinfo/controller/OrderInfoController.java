package com.atguigu.springbootdemo.userinfo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author weiyunhui
 * @since 2024-08-27
 */
@RestController
@RequestMapping("/userinfo/orderInfo")
public class OrderInfoController {

}
