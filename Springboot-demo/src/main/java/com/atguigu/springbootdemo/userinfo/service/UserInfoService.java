package com.atguigu.springbootdemo.userinfo.service;

import com.atguigu.springbootdemo.userinfo.bean.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2024-08-27
 */
public interface UserInfoService extends IService<UserInfo> {

}
